### What is this repository for? ###

* This repository was created a year and a half after the project was installed to address the issue where the system would periodically freeze while building the mosaic.

### How do I get set up? ###

* Platform: Eclipse (current version is Neon) with Proclipsing (v. 0.9a).It is highly recommended that you ensure that the Proclipsing plugin is working before you try to import the mosaic project.
* Setup steps follow:

1. Download Eclipse (I'm using Oxygen, but Neon has been tested and works as well). If you need to install the JRE, do that. For now install v 8u162 (or whatever is most recent) and we will address the correct version later.
2. Install Processing v. 2.2.1 from here: https://processing.org/download/ This is a completely portable program. Just put the contents of the zip folder on your C: drive somewhere.
3. Install Proclipsing from here :https://github.com/ybakos/proclipsing. Follow the install instructions on that page to add Proclipsing as a plugin to Eclipse.
4. In Eclipse, go to Window>Preferences>Proclipsing and point the Processing Path to the location where you just put Processing.
5. Create a new Processing project by going to File>New>Other>Processing>Processing Project.
6. Select all libraries that are detected by the import wizard (dxf, glw, minim, net, pdf, serial, video).
7. Name the project "membershipmosiac" (note the misspelling!) and click Finish.
8. At this point, it is a good idea to test that Processing and Proclipsing are playing together nicely. To do this, open src>[yourProjectName]>[yourProjectName].java. It should be the only .java file in the project.
9. In the setup function, type System.out.println("Hello world!"); and click the Debug button. You should see "Hello World!" appear in the console and a gray PApplet window pop up if everything is working. If this does not work, stop and debug now! It will be very messy debugging Proclipsing while working with the mosaic project.
10. Since the import wizard doesn't work for Processing projects, we have to do a bit of manual copying here. From the git repo, copy the contents of src into your new project's src folder.
11. Do the same for lib>user and xml.
12. Copy the assets folder (see BPI server in [ServerRoot]/ProjectData/Smithsonian Great Hall/TechSupport/Mosaic) and place it in your project next to the src folder.
13. Inside src/data, make a second folder called "assets".
14. From the original assets folder, cut "CutUp", "wheel" and "attract" and paste them to src/data/assets.
15. Now import the mail and encryption libraries: right click on the project in the Package Explorer and go to Properties. 
	- Click "Add JARs"
	- navigate to lib/users and select both the JARs that live there
	- click Apply and Close

You should have a runnable project now!


* This project will not work with any version of Java later than Java 6 due to the video library that is used (gstreamer). Processing comes with its own version of Java and you should be fine with the most recent version for development, but you should make sure Java 6 is installed on the target computer. 
* Deployment instructions: You must export this project via the Eclipse export function - the Proclipsing export does not work. For the export settings, choose "Runnable JAR" as the file type. For the program to run, this JAR file needs to be placed alongside three assets folders: xml, output, and assets. The "assets" folder is very large, so it is not included in the repo. Samples of the output and xml folders are included in the /data folder in the repo. 


