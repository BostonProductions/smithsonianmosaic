package membershipmosiac;

import java.util.ArrayList;
import java.util.Vector;

import processing.core.PApplet;
import processing.core.PFont;
import processing.core.PImage;
import processing.data.XML;
import processing.video.Capture;
//import com.knowcean.si.pki.*;
import java.io.*;
import com.knowcean.si.pki.RSA;

public class MembershipMosiac extends PApplet {

	XML emailXML;
	MailStuff mailer;

	int howManyBoxes;
	ArrayList<Box> theBoxes = new ArrayList<Box>();
	// int numOfImages = 298;
	ArrayList<PImage> images_ST = new ArrayList<PImage>(); // creates the array
															// for the images
	ArrayList<PImage> images_HC = new ArrayList<PImage>(); // creates the array
															// for the images
	ArrayList<PImage> images_AD = new ArrayList<PImage>(); // creates the array
															// for the images
	ArrayList<PImage> images_All = new ArrayList<PImage>(); // creates the array
															// for the images
	ArrayList<PImage> attractImages = new ArrayList<PImage>();
	ArrayList<PImage> attractOverlays = new ArrayList<PImage>();
	ArrayList<PImage> wheels = new ArrayList<PImage>();
	int tileHeight; // the height of a tile
	int tileWidth; // the width of a tile
	public int smallTileHeight = 2;
	public int smallTileWidth = 2;
	int smallWidth;
	int smallHeight;
	PImage mainPic;
	boolean updated;
	Capture video;
	int num = 1;
	String fileName;
	String emailField = "";
	String fName = "";
	String lName = "";
	int whichField = 1;

	boolean choseYet = false;

	boolean showMosaic = false;
	boolean readyToGo = false;

	int currentScreen = 0;

	int isCounting = 5;

	boolean showPriv = false;

	PImage lang1;
	PImage lang2;

	PImage chooseInterest;

	PImage cropped;
	PImage photo;
	PImage photoSmall;

	PImage allReq;
	PImage allReqS;

	PImage chooseAD;
	PImage chooseHC;
	PImage chooseST;
	PImage chooseAll;

	PImage termsScrollBG;
	PImage termsScroller;
	PImage termsBG;
	PImage termsText;
	PImage termsTop;

	PImage startOverSmall;
	PImage crop;
	PImage email;
	PImage wheel;
	PImage cropHandler;
	PImage box1;
	PImage box1che;
	PImage box2;
	PImage box2che;
	PImage emailBox;
	PImage keyboard;
	PImage sayCheese;
	PImage noRetype;
	PImage ok;
	PImage okSend;
	PImage progress1;
	PImage progress2;
	PImage progress3;
	PImage progress4;
	PImage progress4a;
	PImage retake;
	PImage send;
	PImage startCountdown;
	PImage startOver;
	PImage terms;
	PImage up1;
	PImage up2;
	PImage up3;
	PImage yesSend;
	PImage artSwatch;
	PImage historySwatch;
	PImage scienceSwatch;
	PImage allSwatch;
	PImage choose;
	PImage congrats;
	PImage congratsTouch;
	PImage didYouKnow;
	PImage progress;
	PImage instruct;
	PImage visit;
	PImage muse1;
	PImage muse2;
	PImage muse3;
	PImage muse4;
	PImage top1;
	PImage top2;
	PImage top3;
	PImage top4;
	PImage top5;
	PImage thisYour;
	PImage done;
	PImage exit;
	PImage exitRot;
	PImage confirm;
	PImage choice;
	PImage facts;
	PImage fNameBox;
	PImage lNameBox;
	PImage emailBoxActive;
	PImage fNameBoxActive;
	PImage lNameBoxActive;
	PImage fact1;
	PImage fact2;
	PImage fact3;
	PImage welcome;
	PImage beingCreated;
	int last = -1;
	int[] distArray;
	int[] distArray1;
	int[] distArray2;
	int[] distArray3;
	int[] distArray4;
	int[] distArrayAll;
	int howManyImages = -1;
	boolean notRestarted = false;
	boolean box1Checked = false;
	boolean box2Checked = false;
	int textSizeE = 36;
	int textSizeF = 36;
	int textSizeL = 36;
	int cropX;
	int cropY;
	int cropW;
	int cropH;
	int whichFact = 0;
	boolean stopDraw = false;
	int whichMuseWay = 1;
	int maxCount = 200;
	int whichMuse = 1;
	int museTint = 1080;
	int animCount = 0;
	int framesSinceLastFactChange = 0;
	boolean imagesNotLoadedYet = true;
	boolean animIsStarted = false;
	PImage img;

	// Spanish Images

	PImage chooseInterestS;

	PImage croppedS;
	PImage photoS;
	PImage photoSmallS;

	PImage chooseADS;
	PImage chooseHCS;
	PImage chooseSTS;
	PImage chooseAllS;

	PImage startOverSmallS;
	PImage cropS;
	PImage emailS;
	PImage sayCheeseS;
	PImage noRetypeS;
	PImage okS;
	PImage okSendS;
	PImage progress1S;
	PImage progress2S;
	PImage progress3S;
	PImage progress4S;
	PImage progress4aS;
	PImage retakeS;
	PImage sendS;
	PImage startCountdownS;
	PImage startOverS;
	PImage termsS;
	PImage yesSendS;
	PImage chooseS;
	PImage congratsS;
	PImage congratsTouchS;
	PImage didYouKnowS;
	PImage progressS;
	PImage instructS;
	PImage visitS;
	PImage muse1S;
	PImage muse2S;
	PImage muse3S;
	PImage muse4S;
	PImage top1S;
	PImage top2S;
	PImage top3S;
	PImage top4S;
	PImage top5S;
	PImage thisYourS;
	PImage doneS;
	PImage exitS;
	PImage exitRotS;
	PImage confirmS;
	PImage choiceS;
	PImage factsS;
	PImage fact1S;
	PImage fact2S;
	PImage fact3S;
	PImage welcomeS;
	PImage beingCreatedS;

	int boxYOffset;
	int boxXOffset;

	int museTintRate = 5;

	boolean showInst = true;

	int[] lastOnes;

	boolean setBG = true;

	boolean firstTime = true;

	int whichGroup = 0;

	// ControlP5 cp5;

	LoadXML varsXML;

	XML xml_ST;
	XML xml_HC;
	XML xml_AD;
	XML xml_All;
	XML vars;
	Photo[] photos_ST;
	Photo[] photos_HC;
	Photo[] photos_AD;
	Photo[] photos_All;

	int whichImg = 0;
	int whichOv = 0;
	int theTint = 0;
	int ovTint = 0;
	int tintWhichWay = 1;

	int l = 0;

	PImage tiny;

	PFont f;

	int whichLang = 1;

	int timeoutTimer = 0;

	int theX = 0;
	int theY = 0;
	boolean pauseTimer = false;

	float handler1X = 2230;
	float handler1Y = 430;
	float handler2X = 2230 + 192;
	float handler2Y = 430 + 108;
	boolean handler1Moving = false;
	boolean handler2Moving = false;
	boolean boxMoving = false;

	float handler1XSaved = 2230;
	float handler1YSaved = 430;
	float handler2XSaved = 2230 + 192;
	float handler2YSaved = 430 + 108;

	int whichCountFrame = 1;
	int whichArrowFrame = 1;
	boolean mosaicing = false;

	private static String logFileName = ".//pii.txt";
	private static String publicKeyName = "test_public_key.pem";
	private static String privateKeyName = "test_private_key.pem";
	
	private MosaicState mosaicState = new MosaicState();

	int termsPage = 1;
	// String emx = "data/emailXML.xml";

	public static void main(String args[]) {
		println("main");
		println("Hello world!");
		PApplet.main(new String[] { "--location=0,0", "--hide-stop", "--display=1", "membershipmosiac.MembershipMosiac" });
	}

	public void init() {
		println("init");

		if (frame != null) {
			frame.removeNotify();// make the frame not displayable
			frame.setResizable(false);
			frame.setUndecorated(true);
			// println("frame is at "+frame.getLocation());
			frame.addNotify();
		}
		super.init();

		// emailXML = loadXML(emx);
		// println(emailXML);

		varsXML = new LoadXML(this);

		vars = loadXML("../xml/config.xml");

		XML controlsElement = vars.getChild("Controls");
		XML showMouseElement = controlsElement.getChild("ShowMouse");
		varsXML.showMouse = showMouseElement.getIntContent();
		// println(vars);
		XML emailElement = vars.getChild("Email");
		XML fromElement = emailElement.getChild("From");
		varsXML.emailFrom = fromElement.getContent();

		XML usernameElement = emailElement.getChild("Username");
		varsXML.username = usernameElement.getContent();

		XML passwordElement = emailElement.getChild("SMTP");
		String temp = passwordElement.getContent();
		if (temp.equals("yes")) {
			varsXML.useSMTP = true;
		} else {
			varsXML.useSMTP = false;
		}
		// varsXML.password = passwordElement.getContent();

		XML smtpElement = emailElement.getChild("Password");
		varsXML.password = smtpElement.getContent();

		XML replytoElement = emailElement.getChild("ReplyTo");
		varsXML.replyto = replytoElement.getContent();

		XML senderElement = emailElement.getChild("Sender");
		varsXML.sender = senderElement.getContent();

		XML hostElement = emailElement.getChild("Host");
		varsXML.host = hostElement.getContent();

		XML portElement = emailElement.getChild("Port");
		varsXML.port = portElement.getIntContent();

		XML subjectElement = emailElement.getChild("Subject");
		XML lang1SubjectElement = subjectElement.getChild("english");
		varsXML.subject[0] = lang1SubjectElement.getContent();

		XML bodyElement = emailElement.getChild("Body");
		XML lang1BodyElement = bodyElement.getChild("english");
		varsXML.body[0] = lang1BodyElement.getContent();

		XML filePathElement = emailElement.getChild("FilePath");
		varsXML.filePath = filePathElement.getContent();

		XML mosaicElement = vars.getChild("Mosaic");
		XML tileWidthElement = mosaicElement.getChild("TileWidth");
		varsXML.tileWidth = tileWidthElement.getIntContent();

		XML tileHeightElement = mosaicElement.getChild("TileHeight");
		varsXML.tileHeight = tileHeightElement.getIntContent();

		XML numberWithoutRepeatsElement = mosaicElement.getChild("NumberWithoutRepeats");
		varsXML.numberWithoutRepeats = numberWithoutRepeatsElement.getIntContent();

		XML smallSizeWidthElement = mosaicElement.getChild("SmallSizeWidth");
		varsXML.smallSizeWidth = smallSizeWidthElement.getIntContent();

		XML smallSizeHeightElement = mosaicElement.getChild("SmallSizeHeight");
		varsXML.smallSizeHeight = smallSizeHeightElement.getIntContent();

		XML outputSizeWidthElement = mosaicElement.getChild("OutputSizeWidth");
		varsXML.outputSizeWidth = outputSizeWidthElement.getIntContent();

		XML outputSizeHeightElement = mosaicElement.getChild("OutputSizeHeight");
		varsXML.outputSizeHeight = outputSizeHeightElement.getIntContent();

		museTintRate = varsXML.mtr;

		setVariables();
	}

	void setVariables() {
		println("setVariables");
		tileHeight = varsXML.tileHeight;
		tileWidth = varsXML.tileWidth;
		smallHeight = 1920 / tileHeight * 2;
		smallWidth = 1080 / tileWidth * 2;
		// println(smallHeight + ", " + smallWidth);

		lastOnes = new int[varsXML.numberWithoutRepeats];

		for (int v = lastOnes.length - 1; v >= 0; v--) {
			lastOnes[v] = 0;
		}
	}

	public void setup() {
		println("setup");
		size(1080, 2688); // 1080, 2688
		// Ani.init(this);

		mailer = new MailStuff();

		f = createFont("DejaVu Sans Condensed", 16, true);

		images_ST.clear();
		images_HC.clear();
		images_AD.clear();
		images_All.clear();

		video = new Capture(this, 1920, 1080);
		video.start();

		lang1 = loadImage("assets/CutUp/SelectEnglishButton.png");
		lang2 = loadImage("assets/CutUp/SelectSpanishButton.png");
		chooseAD = loadImage("assets/CutUp/ArtDesignEnglishButton.png");
		chooseHC = loadImage("assets/CutUp/HistoryCultureEnglishButton.png");
		chooseST = loadImage("assets/CutUp/ScienceTechEnglishButton.png");
		chooseAll = loadImage("assets/CutUp/EventhingEnglishButton.png");

		termsScrollBG = loadImage("assets/CutUp/termscrollbarbackground.png");
		termsScroller = loadImage("assets/CutUp/termscrollbarnob.png");
		termsBG = loadImage("assets/CutUp/termbackground.png");
		termsText = loadImage("assets/CutUp/termsfulltext.png"); // update
																	// this
		termsTop = loadImage("assets/CutUp/termbackgroundtop.png"); // update
																		// this

		allReq = loadImage("assets/CutUp/allfieldsrequiredEng.png");
		allReqS = loadImage("assets/CutUp/allfieldsrequiredSpanish.png");

		crop = loadImage("assets/CutUp/CropEnglishButton.png");
		email = loadImage("assets/CutUp/EmailphotoEnglishButton.png");
		wheel = loadImage("assets/CutUp/BigLogoProgress.png");
		cropHandler = loadImage("assets/CutUp/CropHandler.png");
		box1 = loadImage("assets/CutUp/CheckBoxEmpty.png");
		box1che = loadImage("assets/CutUp/CheckBoxFill0.png");
		box2 = loadImage("assets/CutUp/CheckBoxEmpty.png");
		box2che = loadImage("assets/CutUp/CheckBoxFill0.png");
		emailBox = loadImage("assets/CutUp/EmailFillBox.png");
		fNameBox = loadImage("assets/CutUp/EmailFNameBox.png");
		lNameBox = loadImage("assets/CutUp/EmailLNameBox.png");
		emailBoxActive = loadImage("assets/CutUp/EmailFillBoxActive.png");
		fNameBoxActive = loadImage("assets/CutUp/EmailFNameBoxActive.png");
		lNameBoxActive = loadImage("assets/CutUp/EmailLNameBoxActive.png");
		keyboard = loadImage("assets/CutUp/KeyBoardWhole.png");
		sayCheese = loadImage("assets/CutUp/LookCameraEnglish.png");
		noRetype = loadImage("assets/CutUp/NoEnglishButton.png");
		ok = loadImage("assets/CutUp/OKEnglishButton.png");
		progress1 = loadImage("assets/CutUp/Progress1English.png");
		progress2 = loadImage("assets/CutUp/Progress2English.png");
		progress3 = loadImage("assets/CutUp/Progress3English.png");
		progress4 = loadImage("assets/CutUp/Progress4EnglishRotated.png");
		progress4a = loadImage("assets/CutUp/Progress4English.png");
		retake = loadImage("assets/CutUp/RetakeEnglishButton.png");
		send = loadImage("assets/CutUp/SendEnglishButton.png");
		startCountdown = loadImage("assets/CutUp/StartCountdownEnglishButton.png");
		startOver = loadImage("assets/CutUp/StartoverEnglishButton.png");
		terms = loadImage("assets/CutUp/termsEnglish2.png");
		up1 = loadImage("assets/CutUp/UpArrow1.png");
		up2 = loadImage("assets/CutUp/UpArrow2.png");
		up3 = loadImage("assets/CutUp/UpArrow3.png");
		yesSend = loadImage("assets/CutUp/YesEnglishButton.png");
		artSwatch = loadImage("assets/CutUp/ArtEnglish.png");
		historySwatch = loadImage("assets/CutUp/HistoryEnglish.png");
		scienceSwatch = loadImage("assets/CutUp/ScienceEnglish.png");
		allSwatch = loadImage("assets/CutUp/EverythingEnglish.png");
		choose = loadImage("assets/CutUp/ChooseEnglishBg.png");
		congrats = loadImage("assets/CutUp/CongratulationsEnglish.png");
		didYouKnow = loadImage("assets/CutUp/DidyouknowEnglish.png");
		progress = loadImage("assets/CutUp/CreatingMosaicProgress.png");
		instruct = loadImage("assets/CutUp/InstructionEnglish.png");
		visit = loadImage("assets/CutUp/JoinEnglish.png");
		muse1 = loadImage("assets/CutUp/MuseumEnglish1.png");
		muse2 = loadImage("assets/CutUp/MuseumEnglish2.png");
		muse3 = loadImage("assets/CutUp/MuseumEnglish3.png");
		muse4 = loadImage("assets/CutUp/MuseumEnglish4.png");
		top1 = loadImage("assets/CutUp/Top1.png");
		top2 = loadImage("assets/CutUp/Top2.png");
		top3 = loadImage("assets/CutUp/Top3.png");
		top4 = loadImage("assets/CutUp/Top4.png");
		top5 = loadImage("assets/CutUp/Top5.png");
		thisYour = loadImage("assets/CutUp/TopEnglish.png");
		done = loadImage("assets/CutUp/DoneEnglishButton.png");
		exit = loadImage("assets/CutUp/ExitEnglishButton.png");
		exitRot = loadImage("assets/CutUp/ExitEnglishButtonRotated.png");
		confirm = loadImage("assets/CutUp/EmailConfirmBg.png");
		okSend = loadImage("assets/CutUp/OKEnglishButton.png");
		choice = loadImage("assets/CutUp/SelectinteresEnglish_5M.png");
		facts = loadImage("assets/CutUp/FactEnglish.png");
		congratsTouch = loadImage("assets/CutUp/CongratulationsEnglishTouch.png");
		startOverSmall = loadImage("assets/CutUp/StartoverSmallEnglishButton.png");
		fact1 = loadImage("assets/CutUp/Fact2English.png");
		fact2 = loadImage("assets/CutUp/Fact3English.png");
		fact3 = loadImage("assets/CutUp/Fact4English.png");
		welcome = loadImage("assets/CutUp/welcomeEnglish.png");
		chooseInterest = loadImage("assets/CutUp/chooseimagesEnglish.png");
		beingCreated = loadImage("assets/CutUp/beingcreatedEnglish.png");

		// Spanish

		chooseADS = loadImage("assets/CutUp/ArtDesignSpanishButton.png");
		chooseHCS = loadImage("assets/CutUp/HistoryCultureSpanishButton.png");
		chooseSTS = loadImage("assets/CutUp/ScienceTechSpanishButton.png");
		chooseAllS = loadImage("assets/CutUp/EventhingSpanishButton.png");

		cropS = loadImage("assets/CutUp/CropSpanishButton.png");
		emailS = loadImage("assets/CutUp/EmailphotoSpanishButton.png");
		sayCheeseS = loadImage("assets/CutUp/LookupSpanish.png");
		noRetypeS = loadImage("assets/CutUp/retypeSpanish.png");
		okS = loadImage("assets/CutUp/OKEnglishButton.png"); // need
		progress1S = loadImage("assets/CutUp/Progress1Spanish.png");
		progress2S = loadImage("assets/CutUp/Progress2Spanish.png");
		progress3S = loadImage("assets/CutUp/Progress3Spanish.png");
		progress4S = loadImage("assets/CutUp/Progress4SpanishRotated.png");
		progress4aS = loadImage("assets/CutUp/Progress4Spanish.png");
		retakeS = loadImage("assets/CutUp/RetakeSpanishButton.png");
		sendS = loadImage("assets/CutUp/SendSpanishButton.png");
		startCountdownS = loadImage("assets/CutUp/StartCountdownSpanishButton.png");
		startOverS = loadImage("assets/CutUp/StartoverSpanishButton.png");
		termsS = loadImage("assets/CutUp/termsSpanish.png");
		yesSendS = loadImage("assets/CutUp/sendSpanish.png");
		chooseS = loadImage("assets/CutUp/ChooseSpanishBg.png");
		congratsS = loadImage("assets/CutUp/ThanksSpanish.png");
		didYouKnowS = loadImage("assets/CutUp/FriendsofsmithsonianSpanish.png");
		instructS = loadImage("assets/CutUp/InstructionSpanish.png");
		visitS = loadImage("assets/CutUp/JoinSpanish.png");
		muse1S = loadImage("assets/CutUp/MuseumSpanish1.png");
		muse2S = loadImage("assets/CutUp/MuseumSpanish2.png");
		muse3S = loadImage("assets/CutUp/MuseumSpanish3.png");
		muse4S = loadImage("assets/CutUp/MuseumSpanish4.png");
		thisYourS = loadImage("assets/CutUp/JointhesmithsonianSpanishTop.png");
		doneS = loadImage("assets/CutUp/DoneSpanishButton.png");
		exitS = loadImage("assets/CutUp/ExitSpanishButton.png");
		exitRotS = loadImage("assets/CutUp/ExitSpanishButtonRotated.png");
		confirmS = loadImage("assets/CutUp/IsYouremailaddressSpanish.png");
		okSendS = loadImage("assets/CutUp/OKEnglishButton.png"); // not used
		choiceS = loadImage("assets/CutUp/ChooseSpanish.png");
		factsS = loadImage("assets/CutUp/MultimuseumSpanish.png");
		congratsTouchS = loadImage("assets/CutUp/thankSpanish.png");
		startOverSmallS = loadImage("assets/CutUp/StartoverSmallSpanishButton.png");
		fact1S = loadImage("assets/CutUp/Fact2Spanish.png");
		fact2S = loadImage("assets/CutUp/Fact3Spanish.png");
		fact3S = loadImage("assets/CutUp/Fact4Spanish.png");
		welcomeS = loadImage("assets/CutUp/welcomeSpanish.png");
		chooseInterestS = loadImage("assets/CutUp/chooseimagesSpanish.png");
		beingCreatedS = loadImage("assets/CutUp/beingcreatedSpanish.png");

		java.io.File folder = new java.io.File("assets/attract");
		System.out.println(folder.getAbsolutePath());
		String[] tileImageFilenames = folder.list();

		// println(tileImageFilenames.length);
		// println(tileImageFilenames[0]);
		for (int i = 0; i < tileImageFilenames.length; i++) {
			println(tileImageFilenames[i]);
			if (tileImageFilenames[i].endsWith("png")) {
				PImage curA = loadImage("assets/attract/" + tileImageFilenames[i]); 
				attractImages.add(curA);
			}
		}

		java.io.File folder2 = new java.io.File("assets/overlays");
		String[] overlayImageFilenames = folder2.list();

		for (int i = 0; i < overlayImageFilenames.length; i++) {
			println(overlayImageFilenames[i]);
			if (overlayImageFilenames[i].endsWith("png")) {
				PImage curB = loadImage("assets/overlays/" + overlayImageFilenames[i]); // loads
																							// the
																							// images
																							// into
																							// the
																							// array
				// curA.resize(tileWidth, tileHeight); //resizes all images to
				// the proper dimensions
				attractOverlays.add(curB);
			}
		}

		java.io.File folder3 = new java.io.File("assets/wheel");
		String[] wheelFilenames = folder3.list();

		for (int i = 0; i < wheelFilenames.length; i++) {
			println(wheelFilenames[i]);
			if (wheelFilenames[i].endsWith("png")) {
				PImage curC = loadImage("assets/wheel/" + wheelFilenames[i]); // loads
																					// the
																					// images
																					// into
																					// the
																					// array
				// curA.resize(tileWidth, tileHeight); //resizes all images to
				// the proper dimensions
				wheels.add(curC);
			}
		}

		drawAttractLoop();

		xml_ST = loadXML("../assets/data_ScienceTechnology.xml");

		XML[] children = xml_ST.getChildren("image");

		xml_HC = loadXML("../assets/data_HistoryCulture.xml");

		XML[] childrenHC = xml_HC.getChildren("image");

		xml_AD = loadXML("../assets/data_ArtDesign.xml");

		XML[] childrenAD = xml_AD.getChildren("image");

		int savei = 0;

		// The size of the array of Bubble objects is determined by the total
		// XML elements named "bubble"
		photos_ST = new Photo[children.length];

		photos_All = new Photo[children.length + childrenHC.length + childrenAD.length];
		// println("children.length = " + children.length);
		for (int i = 0; i < photos_ST.length; i++) {
			// println("looping through xml data ST = " + i);
			XML positionElement = children[i].getChild("fileName");
			String fn = positionElement.getContent();

			PImage cur = loadImage("../assets/ScienceTechnology/" + fn); // loads
																			// the
																			// images
																			// into
																			// the
																			// array
			cur.resize(tileWidth, tileHeight); // resizes all images to the
												// proper dimensions
			rotate(radians(-90));
			images_ST.add(cur);
			images_All.add(cur);
			rotate(radians(0));
			// println("filename = " + fn);
			/// String fn = "null";
			XML redElement = children[i].getChild("red");
			float r = redElement.getFloatContent();

			XML greenElement = children[i].getChild("green");
			float g = greenElement.getFloatContent();

			XML blueElement = children[i].getChild("blue");
			float b = blueElement.getFloatContent();
			////////////////////////////////////////////

			XML redElement1 = children[i].getChild("red1");
			float r1 = redElement1.getFloatContent();

			XML greenElement1 = children[i].getChild("green1");
			float g1 = greenElement1.getFloatContent();

			XML blueElement1 = children[i].getChild("blue1");
			float b1 = blueElement1.getFloatContent();
			////////////////////////////////////////////
			XML redElement2 = children[i].getChild("red2");
			float r2 = redElement2.getFloatContent();

			XML greenElement2 = children[i].getChild("green2");
			float g2 = greenElement2.getFloatContent();

			XML blueElement2 = children[i].getChild("blue2");
			float b2 = blueElement2.getFloatContent();
			////////////////////////////////////////////
			XML redElement3 = children[i].getChild("red3");
			float r3 = redElement3.getFloatContent();

			XML greenElement3 = children[i].getChild("green3");
			float g3 = greenElement3.getFloatContent();

			XML blueElement3 = children[i].getChild("blue3");
			float b3 = blueElement3.getFloatContent();
			////////////////////////////////////////////
			XML redElement4 = children[i].getChild("red4");
			float r4 = redElement4.getFloatContent();

			XML greenElement4 = children[i].getChild("green4");
			float g4 = greenElement4.getFloatContent();

			XML blueElement4 = children[i].getChild("blue4");
			float b4 = blueElement4.getFloatContent();

			photos_ST[i] = new Photo(fn, r, g, b, r1, g1, b1, r2, g2, b2, r3, g3, b3, r4, g4, b4);
			println("savei = " + savei);
			photos_All[savei] = new Photo(fn, r, g, b, r1, g1, b1, r2, g2, b2, r3, g3, b3, r4, g4, b4);
			savei++;
		}

		//////////////////////

		// The size of the array of Bubble objects is determined by the total
		// XML elements named "bubble"
		photos_HC = new Photo[childrenHC.length];
		// println("children.length = " + children.length);
		for (int i = 0; i < photos_HC.length; i++) {
			// println("looping through xml data HC = " + i);
			XML positionElementHC = childrenHC[i].getChild("fileName");
			String fnHC = positionElementHC.getContent();

			PImage curHC = loadImage("../assets/HistoryCulture/" + fnHC); // loads
																			// the
																			// images
																			// into
																			// the
																			// array
			curHC.resize(tileWidth, tileHeight); // resizes all images to the
													// proper dimensions
			rotate(radians(-90));
			images_HC.add(curHC);
			images_All.add(curHC);
			rotate(radians(0));
			// println("filename = " + fn);
			/// String fn = "null";
			XML redElementHC = childrenHC[i].getChild("red");
			float rHC = redElementHC.getFloatContent();

			XML greenElementHC = childrenHC[i].getChild("green");
			float gHC = greenElementHC.getFloatContent();

			XML blueElementHC = childrenHC[i].getChild("blue");
			float bHC = blueElementHC.getFloatContent();
			////////////////////////////////////////////

			XML redElement1HC = childrenHC[i].getChild("red1");
			float r1HC = redElement1HC.getFloatContent();

			XML greenElement1HC = childrenHC[i].getChild("green1");
			float g1HC = greenElement1HC.getFloatContent();

			XML blueElement1HC = childrenHC[i].getChild("blue1");
			float b1HC = blueElement1HC.getFloatContent();
			////////////////////////////////////////////
			XML redElement2HC = childrenHC[i].getChild("red2");
			float r2HC = redElement2HC.getFloatContent();

			XML greenElement2HC = childrenHC[i].getChild("green2");
			float g2HC = greenElement2HC.getFloatContent();

			XML blueElement2HC = childrenHC[i].getChild("blue2");
			float b2HC = blueElement2HC.getFloatContent();
			////////////////////////////////////////////
			XML redElement3HC = childrenHC[i].getChild("red3");
			float r3HC = redElement3HC.getFloatContent();

			XML greenElement3HC = childrenHC[i].getChild("green3");
			float g3HC = greenElement3HC.getFloatContent();

			XML blueElement3HC = childrenHC[i].getChild("blue3");
			float b3HC = blueElement3HC.getFloatContent();
			////////////////////////////////////////////
			XML redElement4HC = childrenHC[i].getChild("red4");
			float r4HC = redElement4HC.getFloatContent();

			XML greenElement4HC = childrenHC[i].getChild("green4");
			float g4HC = greenElement4HC.getFloatContent();

			XML blueElement4HC = childrenHC[i].getChild("blue4");
			float b4HC = blueElement4HC.getFloatContent();

			photos_HC[i] = new Photo(fnHC, rHC, gHC, bHC, r1HC, g1HC, b1HC, r2HC, g2HC, b2HC, r3HC, g3HC, b3HC, r4HC,
					g4HC, b4HC);
			println("savei = " + savei);
			photos_All[savei] = new Photo(fnHC, rHC, gHC, bHC, r1HC, g1HC, b1HC, r2HC, g2HC, b2HC, r3HC, g3HC, b3HC,
					r4HC, g4HC, b4HC);
			savei++;
		}
		///////////////////////////

		///////////////////////////

		// The size of the array of Bubble objects is determined by the total
		// XML elements named "bubble"
		photos_AD = new Photo[childrenAD.length];
		// println("children.length = " + children.length);
		for (int i = 0; i < photos_AD.length; i++) {
			// println("looping through xml data AD = " + i);
			XML positionElementAD = childrenAD[i].getChild("fileName");
			String fnAD = positionElementAD.getContent();

			PImage curAD = loadImage("../assets/ArtDesign/" + fnAD); // loads
																		// the
																		// images
																		// into
																		// the
																		// array
			curAD.resize(tileWidth, tileHeight); // resizes all images to the
													// proper dimensions
			rotate(radians(-90));
			images_AD.add(curAD);
			images_All.add(curAD);
			rotate(radians(0));
			// println("filename = " + fn);
			/// String fn = "null";
			XML redElementAD = childrenAD[i].getChild("red");
			float rAD = redElementAD.getFloatContent();

			XML greenElementAD = childrenAD[i].getChild("green");
			float gAD = greenElementAD.getFloatContent();

			XML blueElementAD = childrenAD[i].getChild("blue");
			float bAD = blueElementAD.getFloatContent();
			////////////////////////////////////////////

			XML redElement1AD = childrenAD[i].getChild("red1");
			float r1AD = redElement1AD.getFloatContent();

			XML greenElement1AD = childrenAD[i].getChild("green1");
			float g1AD = greenElement1AD.getFloatContent();

			XML blueElement1AD = childrenAD[i].getChild("blue1");
			float b1AD = blueElement1AD.getFloatContent();
			////////////////////////////////////////////
			XML redElement2AD = childrenAD[i].getChild("red2");
			float r2AD = redElement2AD.getFloatContent();

			XML greenElement2AD = childrenAD[i].getChild("green2");
			float g2AD = greenElement2AD.getFloatContent();

			XML blueElement2AD = childrenAD[i].getChild("blue2");
			float b2AD = blueElement2AD.getFloatContent();
			////////////////////////////////////////////
			XML redElement3AD = childrenAD[i].getChild("red3");
			float r3AD = redElement3AD.getFloatContent();

			XML greenElement3AD = childrenAD[i].getChild("green3");
			float g3AD = greenElement3AD.getFloatContent();

			XML blueElement3AD = childrenAD[i].getChild("blue3");
			float b3AD = blueElement3AD.getFloatContent();
			////////////////////////////////////////////
			XML redElement4AD = childrenAD[i].getChild("red4");
			float r4AD = redElement4AD.getFloatContent();

			XML greenElement4AD = childrenAD[i].getChild("green4");
			float g4AD = greenElement4AD.getFloatContent();

			XML blueElement4AD = childrenAD[i].getChild("blue4");
			float b4AD = blueElement4AD.getFloatContent();

			photos_AD[i] = new Photo(fnAD, rAD, gAD, bAD, r1AD, g1AD, b1AD, r2AD, g2AD, b2AD, r3AD, g3AD, b3AD, r4AD,
					g4AD, b4AD);
			println("savei = " + savei);
			photos_All[savei] = new Photo(fnAD, rAD, gAD, bAD, r1AD, g1AD, b1AD, r2AD, g2AD, b2AD, r3AD, g3AD, b3AD,
					r4AD, g4AD, b4AD);
			savei++;
		}
		//////////////////////////
		if (video.available() == true) {
			video.read();
		}
		
		mosaicState.Init(this, smallTileWidth, smallTileHeight, varsXML.numberWithoutRepeats);
		// so ugly
		Vector<PImage> stateImages = new Vector<PImage>();
		stateImages.add(fact1);
		stateImages.add(fact2);
		stateImages.add(fact3);
		stateImages.add(fact1S);
		stateImages.add(fact2S);
		stateImages.add(fact3S);
		stateImages.add(exit);
		stateImages.add(exitS);
		stateImages.add(progress3);
		stateImages.add(progress3S);
		mosaicState.setStateImages(stateImages);

	}

	int scrollerOffset = 0;

	public void mousePressed() {
		println("mousePressed");
		timeoutTimer = 0;

		switch (currentScreen) {
		case 0: // attract/choose language
			if (showPriv == false && mouseX > 100 && mouseX < 100 + 360 && mouseY > 2246 && mouseY < 2246 + 180)// 1970
																												// &&
																												// mouseY
																												// <
																												// 2020)
			{
				// println("lang 1 selected");
				setBG = true;
				currentScreen++;
				whichLang = 1;
				pauseTimer = false;
			}
			if (showPriv == false && mouseX > 564 && mouseX < 564 + 360 && mouseY > 2246 && mouseY < 2246 + 180) {
				// println("lang 2 selected");
				setBG = true;
				currentScreen++;
				whichLang = 2;
				pauseTimer = false;
			}
			if (showPriv == true && mouseX > 880 && mouseX < 880 + termsScroller.width && mouseY > scrollerLoc
					&& mouseY < scrollerLoc + termsScroller.height) {
				// println("start scrolling");
				scrollerMoving = true;
				// scrollerLoc = 1920 + 124;
				scrollerOffset = scrollerLoc - mouseY;
			}
			if (showPriv == true && mouseX > 0 && mouseX < 179 && mouseY > 1920 && mouseY < 1920 + 165) {
				showPriv = false;
			}
			if (showPriv == false && mouseX > 520 && mouseX < 1024 && mouseY > 632 && mouseY < 1920 + 768) {
				showPriv = true;
			}

			break;
		case 1: // take photo, remove this add countdown. save graphics on 0
			if (mouseX > 60 && mouseX < 60 + 200 && mouseY > 2540 && mouseY < 2540 + 100) {
				// println("mousePressed 4");
				currentScreen = 0;
				// println("case 2");
				resetAll();
				// showMosaic = !showMosaic;
			}
			if (mouseX > 332 && mouseX < 332 + 361 && mouseY > 2315 && mouseY < 2315 + 181) {
				// println("mousePressed 1");
				// println("case 1");
				showInst = false;
				pauseTimer = true;
				timeoutTimer = 0;
			}

			break;
		case 2:// this should be the retake/crop choice.
			if (mouseX > 60 && mouseX < 60 + 200 && mouseY > 2540 && mouseY < 2540 + 100) {
				// println("mousePressed 4");
				currentScreen = 0;
				// println("case 2");
				resetAll();
				// showMosaic = !showMosaic;
			}
			if (mouseX > 100 && mouseX < 100 + 360 && mouseY > 2246 && mouseY < 2246 + 180) {
				// resetAll();
				showInst = true;

				isCounting = 5;
				whichCountFrame = 1;
				whichArrowFrame = 1;
				currentScreen--;
				// println("case 2");
				setBG = true;
				// showMosaic = !showMosaic;
			}
			if (mouseX > 564 && mouseX < 564 + 360 && mouseY > 2246 && mouseY < 2246 + 180) {
				currentScreen++;
				// println("case 2");
				setBG = true;
				// showMosaic = !showMosaic;
			}
			break;
		case 3: // this should be the crop screen
			if (mouseX > 60 && mouseX < 60 + 200 && mouseY > 2540 && mouseY < 2540 + 100) {
				currentScreen = 0;
				resetAll();
			}
			if (mouseX > 769 && mouseX < 769 + 200 && mouseY > 2533 && mouseY < 2533 + 100) {
				// println("mousePressed 3");
				tiny = createGraphics(smallHeight, smallWidth);
				tiny.get();
				loadPixels();
				tiny.copy(photo, cropX, cropY, cropW, cropH, 0, 0, smallHeight, smallWidth);


				tiny.updatePixels();
				tiny.save("output/tiny_" + year() + month() + day() + hour() + minute() + second() + millis() + ".jpg");
				currentScreen++;
				setBG = true;
			}
			int range = 30;
			if (mouseX > handler1Y - range && mouseX < handler1Y + range && mouseY > handler1X - range
					&& mouseY < handler1X + range) {
				handler1Moving = true;
				handler2Moving = false;
				boxMoving = false;
			}
			if (mouseX > handler2Y - range && mouseX < handler2Y + range && mouseY > handler2X - range
					&& mouseY < handler2X + range) {
				handler2Moving = true;
				handler1Moving = false;
				boxMoving = false;
			}
			if (mouseX > handler1Y && mouseX < handler2Y && mouseY > handler1X && mouseY < handler2X) {
				boxMoving = true;
				boxYOffset = (int) (mouseX - handler1Y);
				boxXOffset = (int) (mouseY - handler1X);
				handler2Moving = false;
				handler1Moving = false;
			}
			break;

		case 4: // this should be the image choice screen
			
			
			
			if (mouseX > 60 && mouseX < 60 + 200 && mouseY > 2540 && mouseY < 2540 + 100 && whichFact == 0) {
				currentScreen = 0;
				resetAll();
			}
			if (choseYet == false && tiny != null) {
				if (mouseX > 172 && mouseX < 172 + 325 && mouseY > 2152 && mouseY < 2152 + 162) {
					if(theBoxes.size() == 0 && !choseYet)
					{
						loadMain();
					}
					choseYet = true;
					// println("mousePressed 4a");
					setBG = true;
					whichGroup = 3;
					mosaicState.setCategory(photos_AD, images_AD);
					showMosaic = !showMosaic;
					background(35, 31, 32);
					if (whichLang == 1) {
						image(wheel, 2083, 262);
						image(didYouKnow, 0, 0);
						image(visit, 1375, 637);
						image(muse1, 929, 201);
						image(facts, 571, 81);
					} else {
						image(wheel, 2083, 262);
						image(thisYourS, 0, 0);
						image(visitS, 0, 1375); // 637);
						image(muse1S, 929, 1080 / 2 - muse1S.width / 2); // 201);
						image(factsS, 571, 81);
					}
					
				}
				else if (mouseX > 512 && mouseX < 512 + 325 && mouseY > 2152 && mouseY < 2152 + 162) {
					if(theBoxes.size() == 0 && !choseYet)
					{
						loadMain();
					}
					choseYet = true;
					// println("mousePressed 4b");
					setBG = true;
					whichGroup = 2;
					mosaicState.setCategory(photos_HC, images_HC);
					showMosaic = !showMosaic;
					background(35, 31, 32);
					if (whichLang == 1) {
						image(wheel, 2083, 262);
						image(thisYour, 0, 0);
						image(visit, 1375, 637);
						image(muse1, 929, 201);
						image(facts, 571, 81);
					} else {
						image(wheel, 2083, 262);
						image(thisYourS, 0, 0);
						image(visitS, 0, 1375); // 637);
						image(muse1S, 929, 1080 / 2 - muse1S.width / 2); // 201);
						image(factsS, 571, 81);
					}

					
				}
				else if (mouseX > 172 && mouseX < 172 + 325 && mouseY > 2328 && mouseY < 2328 + 162) {
					if(theBoxes.size() == 0 && !choseYet)
					{
						loadMain();
					}
					choseYet = true;

					setBG = true;
					whichGroup = 1;
					mosaicState.setCategory(photos_ST, images_ST);
					showMosaic = !showMosaic;
					background(35, 31, 32);
					if (whichLang == 1) {
						image(wheel, 2083, 262);
						image(thisYour, 0, 0);
						image(visit, 1375, 637);
						image(muse1, 929, 201);
						image(facts, 571, 81);
					} else {
						image(wheel, 2083, 262);
						image(thisYourS, 0, 0);
						image(visitS, 0, 1375); // 637);
						image(muse1S, 929, 1080 / 2 - muse1S.width / 2); // 201);
						image(factsS, 571, 81);
					}
					
				}
				else if (mouseX > 512 && mouseX < 512 + 325 && mouseY > 2328 && mouseY < 2328 + 162) {
					if(theBoxes.size() == 0 && !choseYet)
					{
						loadMain();
					}
					choseYet = true;
					// println("mousePressed 4d");
					setBG = true;
					whichGroup = 4;
					mosaicState.setCategory(photos_All, images_All);
					showMosaic = !showMosaic;
					background(35, 31, 32);
					if (whichLang == 1) {
						image(wheel, 2083, 262);
						image(thisYour, 0, 0);
						image(visit, 1375, 637);
						image(muse1, 929, 201);
						image(facts, 571, 81);
					} else {
						image(wheel, 2083, 262);
						image(thisYourS, 0, 0);
						image(visitS, 0, 1375); // 637);
						image(muse1S, 929, 1080 / 2 - muse1S.width / 2); // 201);
						image(factsS, 571, 81);
					}
					
					
				}
			
				
			}
			break;
		case 5: // this should be email choice screen
			if (showPriv == false && mouseX > 60 && mouseX < 60 + 200 && mouseY > 2540 && mouseY < 2540 + 100) {
				// println("mousePressed 4");
				currentScreen = 0;
				// println("case 2");
				resetAll();
				// showMosaic = !showMosaic;
			}
			if (showPriv == false && mouseX > 340 && mouseX < 340 + 360 && mouseY > 2320 && mouseY < 2246 + 180) {
				// println("mousePressed 6a");
				setBG = true;
				currentScreen++;
			}
			if (showPriv == true && mouseX > 880 && mouseX < 880 + termsScroller.width && mouseY > scrollerLoc
					&& mouseY < scrollerLoc + termsScroller.height) {
				// println("start scrolling");
				scrollerMoving = true;
				// scrollerLoc = 1920 + 124;
				scrollerOffset = scrollerLoc - mouseY;
			}
			if (showPriv == false && mouseY < 2320) {
				showPriv = true;
			}

			if (showPriv == true && mouseX > 0 && mouseX < 179 && mouseY > 1920 && mouseY < 1920 + 165) {
				showPriv = false;
			}
			break;


		case 6: // this should be the disclaimer screen
			if (mouseX > 37 && mouseX < 37 + 200 && mouseY > 1920 + 636 && mouseY < 1920 + 636 + 100) {
				// println("mousePressed 4");
				currentScreen = 0;
				// println("case 2");
				resetAll();
				// showMosaic = !showMosaic;619, 2540
			}
			if (box1Checked == true && box2Checked == true && mouseX > 793 && mouseX < 793 + 200 && mouseY > 1920 + 636
					&& mouseY < 1920 + 636 + 100) {
				// println("mousePressed 6");
				currentScreen++;
				// println("case 2");
				setBG = true;
			}
			if (mouseX > 271 && mouseX < 271 + 25 && mouseY > 1920 + 670 && mouseY < 1920 + 670 + 25) {
				box1Checked = !box1Checked;
			}
			if (mouseX > 271 && mouseX < 271 + 25 && mouseY > 1920 + 711 && mouseY < 1920 + 711 + 25) {
				box2Checked = !box2Checked;
			}
			if (termsPage < 13) {
				if (mouseX > 658 && mouseX < 658 + 146 && mouseY > 1920 + 515 && mouseY < 1920 + 515 + 74) {
					termsPage++;
				}
			}
			if (termsPage > 1) {
				if (mouseX > 224 && mouseX < 224 + 146 && mouseY > 1920 + 515 && mouseY < 1920 + 515 + 74) {
					termsPage--;
				}
			}
			break;
		case 7: // this should be the email input screen
			if (mouseX > 60 && mouseX < 60 + 200 && mouseY > 2054 && mouseY < 2054 + 100) {
				// println("mousePressed 4");
				currentScreen = 0;
				// println("case 2");
				resetAll();
				// showMosaic = !showMosaic;
			}
			if (mouseY > 2225 && mouseY < 2225 + 72) {
				if (mouseX > 34 && mouseX < 34 + 67) {
					addChar("1");
				}
				if (mouseX > 133 && mouseX < 133 + 67) {
					addChar("2");
				}
				if (mouseX > 231 && mouseX < 231 + 67) {
					addChar("3");
				}
				if (mouseX > 330 && mouseX < 330 + 67) {
					addChar("4");
				}
				if (mouseX > 429 && mouseX < 429 + 67) {
					addChar("5");
				}
				if (mouseX > 528 && mouseX < 528 + 67) {
					addChar("6");
				}
				if (mouseX > 627 && mouseX < 627 + 67) {
					addChar("7");
				}
				if (mouseX > 726 && mouseX < 726 + 67) {
					addChar("8");
				}
				if (mouseX > 825 && mouseX < 825 + 67) {
					addChar("9");
				}
				if (mouseX > 924 && mouseX < 924 + 67) {
					addChar("0");
				}
			}
			if (mouseY > 2335 && mouseY < 2335 + 72) {
				if (mouseX > 82 && mouseX < 82 + 67) {
					addChar("q");
				}
				if (mouseX > 170 && mouseX < 170 + 67) {
					addChar("w");
				}
				if (mouseX > 258 && mouseX < 258 + 67) {
					addChar("e");
				}
				if (mouseX > 346 && mouseX < 346 + 67) {
					addChar("r");
				}
				if (mouseX > 435 && mouseX < 435 + 67) {
					addChar("t");
				}
				if (mouseX > 523 && mouseX < 523 + 67) {
					addChar("y");
				}
				if (mouseX > 611 && mouseX < 611 + 67) {
					addChar("u");
				}
				if (mouseX > 699 && mouseX < 699 + 67) {
					addChar("i");
				}
				if (mouseX > 787 && mouseX < 787 + 67) {
					addChar("o");
				}
				if (mouseX > 875 && mouseX < 875 + 67) {
					addChar("p");
				}
			}
			if (mouseY > 2412 && mouseY < 2412 + 72) {
				if (mouseX > 127 && mouseX < 127 + 67) {
					addChar("a");
				}
				if (mouseX > 215 && mouseX < 215 + 67) {
					addChar("s");
				}
				if (mouseX > 303 && mouseX < 303 + 67) {
					addChar("d");
				}
				if (mouseX > 391 && mouseX < 391 + 67) {
					addChar("f");
				}
				if (mouseX > 479 && mouseX < 479 + 67) {
					addChar("g");
				}
				if (mouseX > 567 && mouseX < 567 + 67) {
					addChar("h");
				}
				if (mouseX > 655 && mouseX < 655 + 67) {
					addChar("j");
				}
				if (mouseX > 743 && mouseX < 743 + 67) {
					addChar("k");
				}
				if (mouseX > 831 && mouseX < 831 + 67) {
					addChar("l");
				}
			}
			if (mouseY > 2504 && mouseY < 2504 + 72) {
				if (mouseX > 82 && mouseX < 82 + 67) {
					addChar("@");
				}
				if (mouseX > 170 && mouseX < 170 + 67) {
					addChar("z");
				}
				if (mouseX > 258 && mouseX < 258 + 67) {
					addChar("x");
				}
				if (mouseX > 346 && mouseX < 346 + 67) {
					addChar("c");
				}
				if (mouseX > 435 && mouseX < 435 + 67) {
					addChar("v");
				}
				if (mouseX > 523 && mouseX < 523 + 67) {
					addChar("b");
				}
				if (mouseX > 611 && mouseX < 611 + 67) {
					addChar("n");
				}
				if (mouseX > 699 && mouseX < 699 + 67) {
					addChar("m");
				}
				if (mouseX > 787 && mouseX < 787 + 144) {
					removeChar();
				}
			}
			if (mouseY > 2598 && mouseY < 2598 + 72) {
				if (mouseX > 34 && mouseX < 34 + 67) {
					addChar("_");
				}
				if (mouseX > 130 && mouseX < 130 + 67) {
					addChar("-");
				}
				if (mouseX > 218 && mouseX < 218 + 67) {
					addChar(".");
				}
				if (mouseX > 307 && mouseX < 307 + 514) {
					addChar(" ");
				}
				if (mouseX > 844 && mouseX < 844 + 144) {
					addChar(".com");
				}
			}
			if (mouseX > 271 && mouseX < 271 + 506 && mouseY > 1920 + 189 && mouseY < 1920 + 189 + 44) {
				whichField = 1;
			}
			if (mouseX > 271 && mouseX < 271 + 248 && mouseY > 1920 + 134 && mouseY < 1920 + 134 + 44) {
				whichField = 2;
			}
			if (mouseX > 529 && mouseX < 529 + 248 && mouseY > 1920 + 134 && mouseY < 1920 + 134 + 44) {
				whichField = 3;
			}
			if (emailField.equals("")) {
				// println("don't send yet");
			} else {
				if (mouseX > 785 && mouseX < 785 + 200 && mouseY > 1920 + 134 && mouseY < 1920 + 134 + 100) {

					currentScreen++;
				}
			}
			break;
		case 8: // this should be the email check screen
			if (mouseX > 60 && mouseX < 60 + 200 && mouseY > 2540 && mouseY < 2540 + 100) {
				// println("mousePressed 4");
				currentScreen = 0;
				// println("case 2");
				resetAll();
				// showMosaic = !showMosaic;
			}
			if (mouseX > 611 && mouseX < 611 + 200 && mouseY > 2344 && mouseY < 2344 + 100) {
				// println("mousePressed 8");
				hasSent = true;
				draw();
				thread("saveEmailXML");
				currentScreen++;
			}
			if (mouseX > 213 && mouseX < 213 + 200 && mouseY > 2344 && mouseY < 2344 + 100) {
				// println("mousePressed 8");
				currentScreen--;
			}
			break;
		case 9: // this should be the congrats screen
			if (mouseX > 60 && mouseX < 60 + 200 && mouseY > 2540 && mouseY < 2540 + 100) {
				// println("mousePressed 4");
				currentScreen = 0;
				// println("case 2");
				resetAll();
				// showMosaic = !showMosaic;
			}
			break;
		}
	}

	void removeChar() {
		println("removeChar");
		textSizeE = 36;
		textSizeF = 36;
		textSizeL = 36;
		switch (whichField) {
		case 1:
			if (emailField.length() > 0) {
				emailField = emailField.substring(0, emailField.length() - 1);
			}
			// println(emailField);
			break;
		case 2:
			if (fName.length() > 0) {
				fName = fName.substring(0, fName.length() - 1);
			}
			break;
		case 3:
			if (lName.length() > 0) {
				lName = lName.substring(0, lName.length() - 1);
			}
			break;
		}
	}

	void addChar(String s) {
		println("addChar");
		switch (whichField) {
		case 1:
			emailField += s;
			// println(emailField);
			break;
		case 2:
			fName += s;
			break;
		case 3:
			lName += s;
			break;
		}
	}

	public void saveEmailXML() {
		println("saveEmailXML");
		encryptAndWrite(emailField + "," + fName + "," + lName);
		mailer.sendMail(fileName, emailField, varsXML.emailFrom, varsXML.username, varsXML.password, varsXML.host,
				varsXML.port, varsXML.subject[l], varsXML.body[l], "output", varsXML.sender, varsXML.replyto,
				varsXML.useSMTP);

		// println("try to save pii");

		// XML per = emailXML.addChild("person");
		// XML fn = per.addChild("firstName");
		// fn.setContent(fName);
		// XML ln = per.addChild("lastName");
		// ln.setContent(lName);
		// XML em = per.addChild("email");
		// em.setContent(emailField);
		// saveXML(emailXML,emx);
		
	}

	/**
	 * A utility function to write strings to logfile. Assumed that the passed
	 * string will include a newline character at the end (this function doesn't
	 * add it)
	 * 
	 * @param s
	 *            A string to be written to file
	 */
	private static void writeSomething(String s) {
		println("writeSomething");
		System.out.println("Writing to log file named " + logFileName);
		try {
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(logFileName, true)));
			out.println(s);
			out.close();
		} catch (IOException e) {
			System.out.println(e);
		}
	}

	/**
	 * Takes a string, adds a newline character to the end, encrypts it, and
	 * appends it to a log file
	 * 
	 * @param myString
	 *            the string you want to encrypt and log.
	 */
	public static void encryptAndWrite(String myString) {
		println("encryptAndWrite");
		System.out.println("Using public key " + publicKeyName);
		String s = new String(myString + "\n");
		String encrypted = new String("");
		 RSA rsa = new RSA();
		 try {
		 rsa.initPublicKey(publicKeyName);
		 encrypted = rsa.encrypt(s);
		 } catch (Exception e) {
		 System.out.println(e);
		 }

		writeSomething(encrypted);
	}

	void takeImage() {
		println("takeImage");
		setBG = true;
		// showMosaic = !showMosaic;
		// if (showMosaic == true)
		// {
		// loadMain();
		// }
		photo = video.get(0, 0, 1920, 1080);
		image(photo, -1920, 0);
		mosaicState.ClearShuffledBoxes(); // I'm so sorry this goes here. Debugging on the fly
		photoSmall = createGraphics(varsXML.smallSizeHeight, varsXML.smallSizeWidth);
		photoSmall.get();
		loadPixels();
		photoSmall.copy(photo, 0, 0, 1920, 1080, 0, 0, varsXML.smallSizeHeight, varsXML.smallSizeWidth);
		photoSmall.updatePixels();
		// image(photoSmall, -1920, 0);
		photo.save("output/screenshot_" + year() + month() + day() + hour() + minute() + second() + millis() + ".jpg");
		photoSmall.save(
				"output/photoSmall_" + year() + month() + day() + hour() + minute() + second() + millis() + ".jpg");

		currentScreen++;
		pauseTimer = false;
	}

	void resetAll() {
		println("resetAll");
		setVariables();
		scrollerLoc = 1920 + 124;
		textLoc = 1920;
		termsPage = 1;
		timeoutTimer = 0;
		currentScreen = 0;
		firstTime = true;
		showMosaic = false;
		readyToGo = false;
		setBG = true;
		whichGroup = 0;
		whichLang = 1;
		isCounting = 5;
		whichCountFrame = 1;
		whichArrowFrame = 1;
		showInst = true;
		box1Checked = false;
		box2Checked = false;
		mosaicing = false;
		img = null;
		photo = null;
		tiny = null;
		photoSmall = null;
		whichField = 1;
		emailField = "";
		lName = "";
		fName = "";
		handler1X = handler1XSaved;
		handler1Y = handler1YSaved;
		handler2X = handler2XSaved;
		handler2Y = handler2YSaved;
		choseYet = false;
		drawnOnce = false;
		animCount = 0;
		imagesNotLoadedYet = true;
		animIsStarted = false;
		whichMuse = 1;
		museTint = 0;
		whichMuseWay = 1;
		textSizeE = 36;
		textSizeF = 36;
		textSizeL = 36;
		theBoxes.clear();
		pauseTimer = true;
		timeoutTimer = 0;
		hasSent = false;
		stopDraw = false;
		whichFact = 0;
		notRestarted = false;
		showPriv = false;
		framesSinceLastFactChange = 0;
	}

	public void draw() {
		// println("frameRate = " + frameRate);
		if (pauseTimer == false) {
			timeoutTimer++;
		}
		if (timeoutTimer > 6000) {
			currentScreen = 0;
			resetAll();
		}
		if (varsXML.showMouse == 0) {
			noCursor();
		}
		tint(255, 255);

		switch (currentScreen) {
		case 0:
			drawAttractLoop();
			break;
		case 1:
			drawTakePhoto();
			break;
		case 2:
			chooseWhat();
			break;
		case 3:
			drawCrop();
			break;
		case 4:
			drawMosaic();
			break;
		case 5:
			drawEmailChoice();
			break;
		case 6:
			//drawTerms();
			//As far as I can tell, this state is completely unused
			setBG = true;
			currentScreen++;
			break;
		case 7:
			drawEmail();
			break;
		case 8:
			drawConfirm();
			break;
		case 9:
			drawCongrats();
			break;
		default:
			background(0);
			break;
		}

	}

	int scrollerLoc = 1920 + 124;
	int textLoc = 1920;

	// *********************************************Attract*******************************************//
	void drawAttractLoop() {
		// pauseTimer = true;
		// if (setBG == true) {
		// setBG = false;
		background(35, 31, 32);
		// }

		// textFont(f,36);
		// fill(0);
		// text("Choose a Language", 100, 2200);
		tint(255, 255);
		if (whichLang == 1) {
			image(welcome, 0, 1920);
			image(lang1, 100, 2246);
			image(lang2, 564, 2246);
		} else {
			image(welcomeS, 0, 1920);
			image(lang1, 100, 2246);
			image(lang2, 564, 2246);
		}

		if (showPriv == true) {
			image(termsBG, 0, 1920);
			if (scrollerMoving == true) {
				scrollerLoc = mouseY + scrollerOffset;
				if (scrollerLoc > 1920 + 124 + termsScrollBG.height - termsScroller.height) {
					scrollerLoc = 1920 + 124 + termsScrollBG.height - termsScroller.height;
				}
				if (scrollerLoc < 1920 + 124) {
					scrollerLoc = 1920 + 124;
				}
				// println("scrollerMoving = true");
				// println("scrollerOffset = " + scrollerOffset);
				// println("mouseY = " + mouseY);
				// image(termsScroller, 880, scrollerLoc);
				float rat = (float) ((float) (scrollerLoc) - (float) (1920 + 124))
						/ (float) (termsScrollBG.height - termsScroller.height);
				int topNum = scrollerLoc - (1920 + 124);
				// println("topNum = " + topNum);
				// println("termsScroller.height = " + termsScroller.height);
				// println("rat = " + rat);
				textLoc = (int) (1920 - ((termsText.height - 768) * rat));
				// println("textLoc = " + textLoc);
				// println("termsText.height = " + termsText.height);
			}
			// else
			// {

			image(termsText, 0, textLoc);
			image(termsTop, 0, 1920);
			image(termsScrollBG, 880, 124 + 1920);
			image(termsScroller, 886, scrollerLoc);
			// }
		}

		if (whichImg == 0) {
			tint(255, 255);
			image(attractImages.get(attractImages.size() - 1), 0, 0);
			tint(255, theTint);
			image(attractImages.get(whichImg), 0, 0);
		}
		if (whichImg > 0) {
			tint(255, 255);
			image(attractImages.get(whichImg - 1), 0, 0);
			tint(255, theTint);
			image(attractImages.get(whichImg), 0, 0);
		}

		tint(255, ovTint);
		image(attractOverlays.get(whichOv), 0, 0);

		ovTint += tintWhichWay;
		// println("ovTint = " + ovTint);

		if (ovTint > 255) {
			tintWhichWay *= -1;
			ovTint = 255;
		}
		if (ovTint < 0) {
			whichOv++;
			tintWhichWay *= -1;
			ovTint = 0;

			if (whichOv > attractOverlays.size() - 1) {
				whichOv = 0;
			}
		}

		// println("whichOv = " + whichOv);
		theTint++;
		if (theTint > 255) {
			theTint = 0;
			whichImg++;
			if (whichImg > attractImages.size() - 1) {
				whichImg = 0;
			}
		}
	}

	// *********************************************Take
	// Photo*******************************************//
	void drawTakePhoto() {
		scale(1, -1);
		if (setBG == true) {
			// setBG = false;
			background(35, 31, 32);
		}

		if (video.available() == true) {
			video.read();
			video.loadPixels();
		}
		if (showMosaic != true) {
			rotate(radians(-90));
			image(video, 0, 0);
			if (readyToGo == false) {
				readyToGo = true;
			}
			rotate(radians(0));
		} else if (readyToGo == true) {
			readyToGo = false;
		}

		rotate(radians(0));
		scale(1, 1);

		if (isCounting == 5) {
			image(top5, 0, 0);
		}
		if (isCounting == 4) {
			image(top4, 0, 0);
		}
		if (isCounting == 3) {
			image(top3, 0, 0);
		}
		if (isCounting == 2) {
			image(top2, 0, 0);
		}
		if (isCounting == 1) {
			image(top1, 0, 0);
		}
		if (isCounting == 0) {
			takeImage();
		}
		if (whichLang == 1) {
			image(sayCheese, 1920, 0);
		} else {
			image(sayCheeseS, 1920, 0);
		}
		if (showInst == true) {
			if (whichLang == 1) {
				image(instruct, 800, 1080 - 864);
				image(startCountdown, 2315, 332);
			} else {
				image(instructS, 800, 1080 - 864);
				image(startCountdownS, 2315, 332);
			}
		} else {
			whichCountFrame++;
			if (whichCountFrame > 15 && whichCountFrame < 30) {
				isCounting = 4;
			}
			if (whichCountFrame > 30 && whichCountFrame < 45) {
				isCounting = 3;
			}
			if (whichCountFrame > 45 && whichCountFrame < 60) {
				isCounting = 2;
			}
			if (whichCountFrame > 60 && whichCountFrame < 75) {
				isCounting = 1;
			}
			if (whichCountFrame > 75) {
				isCounting = 0;
			}
		}
		if (whichArrowFrame < 5) {
			image(up1, 2153, 172);
		}
		if (whichArrowFrame >= 5 && whichArrowFrame < 10) {
			image(up2, 2153, 172);
		}
		if (whichArrowFrame >= 10 && whichArrowFrame < 15) {
			image(up3, 2153, 172);
		}
		whichArrowFrame++;
		if (whichArrowFrame >= 15) {
			whichArrowFrame = 1;
		}

		if (whichLang == 1) {
			image(progress1, 1920, 0);
			image(exitRot, 2540, 60);
		} else {
			image(progress1S, 1920, 0);
			image(exitRotS, 2540, 60);
		}

		// textFont(f,36);
		// fill(0);
		// text("Take Photo", 100, 2200);
	}

	// *********************************************Choose*******************************************//
	void chooseWhat() {
		
		scale(1, -1);
		if (setBG == true) {
			// setBG = false;
			background(35, 31, 32);
		}

		rotate(radians(-90));
		image(photo, 0, 0);

		rotate(radians(0));
		if (whichLang == 1) {
			image(retake, 2246, 100);
			image(crop, 2246, 564);
			image(progress1, 1920, 0);
			image(exitRot, 2540, 60);
		} else {
			image(retakeS, 2246, 100);
			image(cropS, 2246, 564);
			image(progress1S, 1920, 0);
			image(exitRotS, 2540, 60);
		}
	}

	boolean scrollerMoving = false;

	public void mouseReleased() {
		println("mouseReleased");
		handler1Moving = false;
		handler2Moving = false;
		boxMoving = false;
		scrollerMoving = false;
	}

	float initRatio = 1.92f / 1.08f;

	// *********************************************Crop*******************************************//
	void drawCrop() {

		// println("drawCrop");
		scale(1, -1);
		if (setBG == true) {
			// setBG = false;
			background(35, 31, 32);
		}

		rotate(radians(-90));
		image(photo, 0, 0);

		rotate(radians(0));
		// image(photoSmall, -1920, 0);
		// image(tiny, 0, 0);
		// image(photo, 2000, 300);
		image(photoSmall, 2044, 318);
		if (whichLang == 1) {
			image(done, 2533, 769);
			image(progress2, 1920, 0);
		} else {
			image(doneS, 2533, 769);
			image(progress2S, 1920, 0);
		}

		float savedX = handler1X;
		float savedY = handler1Y;
		float savedX2 = handler2X;
		float savedY2 = handler2Y;
		float w = handler2X - handler1X;
		float l = handler2Y - handler1Y;
		if (boxMoving == true) {
			// println("handler 1 is moving");
			handler1Y = mouseX - boxYOffset;
			handler1X = mouseY - boxXOffset;
			handler2Y = handler1Y + l;
			handler2X = handler1X + w;
			if (handler2Y > 318 + varsXML.smallSizeWidth) {
				handler1Y = savedY;
				handler2Y = handler1Y + l;
			}
			if (handler2X > 2044 + varsXML.smallSizeHeight) {
				handler1X = savedX;
				handler2X = handler1X + w;
			}

			if (handler1X < 2044) {
				handler1X = 2044;
				handler2X = handler1X + w;
			}
			if (handler1X > 2044 + varsXML.smallSizeHeight - w) {
				handler1X = 2044 + varsXML.smallSizeHeight - w;
				handler2X = 2044 + varsXML.smallSizeHeight;
			}

			if (handler1Y < 318) {
				handler1Y = 318;
				handler2Y = handler1Y + l;
			}
			if (handler1Y > 318 + varsXML.smallSizeWidth - l) {
				handler1Y = 318 + varsXML.smallSizeWidth - l;
				handler2Y = 318 + varsXML.smallSizeWidth;
			}

		}
		if (handler2Moving == true) {
			// println("handler 2 is moving");
			handler2Y = mouseX;
			handler2X = mouseY;
		}
		if (handler1Moving == true) {
			// println("handler 2 is moving");
			handler1Y = mouseX;
			handler1X = mouseY;
		}

		float ratio;
		initRatio = 1.92f / 1.08f; // ARGH. If I tell you to be a float, be a
									// float! You don't need a decimal point to
									// prove it.
		// println("************************** " + initRatio);
		if (handler1Moving == true) {
			// println("checking if handler 1 is moving");
			ratio = (handler2X - handler1X) / (handler2Y - handler1Y);
			if (initRatio < ratio) {
				handler1Y = -(((handler2X - handler1X) / initRatio) - handler2Y);
			} else {
				handler1X = -(((handler2Y - handler1Y) * initRatio) - handler2X);
			}
		}
		if (handler2Moving == true) {
			// println("checking if handler 2 is moving");
			ratio = (handler2X - handler1X) / (handler2Y - handler1Y);

			if (initRatio < ratio) {
				handler2Y = ((handler2X - handler1X) / initRatio) + handler1Y;
			} else {
				handler2X = ((handler2Y - handler1Y) * initRatio) + handler1X;
			}
		}

		// float w2 = handler2X - handler1X;
		// float l2 = handler2Y - handler1Y;

		if (handler2X < handler1X + 48) {
			handler2X = handler1X + 48;
		}
		if (handler2X > 2044 + varsXML.smallSizeHeight) {
			handler2X = 2044 + varsXML.smallSizeHeight;
			handler1X = savedX;
			handler2Y = ((handler2X - handler1X) / initRatio) + handler1Y;
			// println("handler2Y = " + handler2Y);
		}
		if (handler1X < 2044) {
			handler1X = 2044;
			handler2X = savedX2;
			handler1Y = -(((handler2X - handler1X) / initRatio) - handler2Y);
		}

		if (handler2Y < handler1Y + 27) {
			handler2Y = handler1Y + 27;
		}
		if (handler2Y > 318 + varsXML.smallSizeWidth) {
			handler2Y = 318 + varsXML.smallSizeWidth;
			handler1Y = savedY;
			handler2X = ((handler2Y - handler1Y) * initRatio) + handler1X;
		}
		if (handler1Y < 318) {
			handler1Y = 318;
			handler2Y = savedY2;
			handler1X = -(((handler2Y - handler1Y) * initRatio) - handler2X);
		}

		setOverlay(handler1Y, handler1X, handler2X - handler1X, handler2Y - handler1Y);
		noStroke();
		fill(34, 144, 207);
		ellipseMode(CENTER);
		ellipse(handler1X, handler1Y, 20, 20);
		ellipse(handler2X, handler2Y, 20, 20);

		if (whichLang == 1) {
			image(exitRot, 2540, 60);
		} else {
			image(exitRotS, 2540, 60);
		}
	}

	void setOverlay(float locX, float locY, float w, float h) {
		println("setOverlay");
		float rat = 1920.0f / varsXML.smallSizeHeight;
		// println("rat = " + rat);
		noStroke();
		fill(0, 0, 0, 150);
		rect(2044, 318, locY - 2044, varsXML.smallSizeWidth);
		// fill(255, 0, 0, 100);
		rect(2044 + (locY - 2044), 318, w, locX - 318);
		// fill(0, 255, 0, 100);
		rect(2044 + (locY - 2044) + w, 318, varsXML.smallSizeHeight - (locY - 2044) - w, varsXML.smallSizeWidth);
		// fill(0, 0, 255, 100);
		rect(2044 + (locY - 2044), 318 + h + (locX - 318), w, varsXML.smallSizeWidth - (locX - 318) - h);

		rect(0, 0, (locY - 2044) * rat, 1080);
		// fill(255, 0, 0, 100);
		rect((locY - 2044) * rat, 0, w * rat, (locX - 318) * rat);
		// fill(0, 255, 0, 100);
		rect(((locY - 2044) * rat) + (w * rat), 0, 1920 - ((locY - 2044) * rat) - (w * rat), 1080);
		// fill(0, 0, 255, 100);
		rect((locY - 2044) * rat, (h * rat) + ((locX - 318) * rat), w * rat, 1080 - ((locX - 318) * rat) - (h * rat));

		stroke(255, 255, 255, 255);
		noFill();
		rect(locY, locX, w, h);
		rect((locY - 2044) * rat, (locX - 318) * rat, w * rat, h * rat);
		cropX = round((locY - 2044) * rat);
		cropY = round((locX - 318) * rat);
		cropW = round(w * rat);
		cropH = round(h * rat);
		// println("locX, locY, w, h = " + locX + ", " + locY + ", " + w + ", "
		// + h);
	}

	boolean drawnOnce = false;
	int howOften = 1;
	int nowYet = 0;

	// *********************************************Mosaic*******************************************//
	void drawMosaic() {

		if (setBG == true) {
			setBG = false;
			background(35, 31, 32);
		}

		if (mosaicing == false) {
			if (whichLang == 1) {
				image(choice, 0, 0);
				image(chooseInterest, 0, 1920);
				image(chooseAD, 172, 2152);
				image(chooseHC, 512, 2152);
				image(chooseST, 172, 2329);
				image(chooseAll, 512, 2329);
			} else {
				image(choiceS, 0, 0);
				image(chooseInterestS, 0, 1920);
				image(chooseADS, 172, 2152);
				image(chooseHCS, 512, 2152);
				image(chooseSTS, 172, 2329);
				image(chooseAllS, 512, 2329);
			}
			// image(exit, 774, 2538);
		} else {
			if (animIsStarted == true) {
				background(35, 31, 32);
				if (whichLang == 1) {
					image(beingCreated, 0, 1920);
					image(wheel, 262, 2013);
					image(thisYour, 0, 0);
					image(visit, 637, 1375);
					image(facts, 81, 571);
					image(progress, 47, 1656);
				} else {
					image(beingCreatedS, 0, 1920);
					image(wheel, 262, 2013);
					image(thisYourS, 0, 0);
					image(visitS, 0, 1375); // 637);
					image(factsS, 81, 571);
					image(progress, 47, 1656);
				}
				// image(exit, 774, 2538);
			}
			else
			{
				if(mosaicState.isMosaicSourceEmpty())
				{
					mosaicState.setShuffledBoxes(this.shuffleArray(theBoxes));
				}	
	
				if(!mosaicState.isMosaicComplete())
				{
					mosaicState.showNextImage(whichLang);
				}
				else
				{
					
					allDone();
					System.out.println("Done with mosaic! Advance to next screen");
					return;
				}
			}
			
		}
		// rotate(radians(0));
		if (firstTime == true) {
			firstTime = false;
		}

		


		if (animIsStarted == true) {
			if (nowYet >= howOften) {
				animCount++;
			}
			nowYet++;
			if (nowYet > howOften) {
				nowYet = 0;
			}
			museTint += museTintRate * whichMuseWay;
			if (museTint > 255) {
				museTint = 255;
				whichMuseWay *= -1;
				// museTint = 1080;

			}
			if (museTint < 0) {
				museTint = 0;
				whichMuseWay *= -1;
				whichMuse++;
				if (whichMuse > 4) {
					whichMuse = 1;
				}
			}
			tint(255, museTint);
			if (whichLang == 1) {
				switch (whichMuse) {
				case 1:
					image(muse1, 0, 929);
					break;
				case 2:
					image(muse2, 0, 929);
					break;
				case 3:
					image(muse3, 0, 929);
					break;
				case 4:
					image(muse4, 0, 929);
					break;
				}
				
				
			} else {

				switch (whichMuse) {
				case 1:
					image(muse1S, 0, 929);
					break;
				case 2:
					image(muse2S, 0, 929);
					break;
				case 3:
					image(muse3S, 0, 929);
					break;
				case 4:
					image(muse4S, 0, 929);
					break;
				}
				
			}
			tint(255, 255);
			fill(255);
			
			if(whichLang == 1)
			{
				image(progress3, 0, 1920);
				image(exit, 60, 2540);
			}
			else
			{
				image(progress3S, 0, 1920);
				image(exitS, 60, 2540);
			}
			noStroke();
			int wid = round(4.56f * animCount);
			rect(81, 1705, wid, 70);
			int wid2 = round(1.195f * animCount);
			if (wid2 > wheels.size() - 1) {
				wid2 = wheels.size() - 1;
			}
			image(wheels.get(wid2), 16, 1882);

			if (animCount >= maxCount && imagesNotLoadedYet == true) {
				background(35, 31, 32);

				if (whichLang == 1) {
					
				} else {
					
				}

				imagesNotLoadedYet = false;
				
				animIsStarted = false;
			}

		}

		
	}

	// *********************************************Email
	// Choice*******************************************//
	void drawEmailChoice() {
		// println("drawEmailChoice");
		if (setBG == true) {
			// setBG = false;
			background(35, 31, 32);
		}
		
		if (img != null) {
			// rotate(radians(-90));
			image(img, 0, 0);
			// rotate(radians(0));
		}
		// textFont(f,36);
		// fill(0);
		// text("Send Email", 100, 2200);
		if (whichLang == 1) {
			image(terms, 0, 1920);
			image(progress4, 0, 1920);
			image(email, 340, 2320);
			image(exit, 60, 2540);
		} else {
			image(termsS, 0, 1920);
			image(progress4S, 0, 1920);
			image(emailS, 340, 2320);
			image(exitS, 60, 2540);
		}

		if (showPriv == true) {
			image(termsBG, 0, 1920);
			if (scrollerMoving == true) {
				scrollerLoc = mouseY + scrollerOffset;
				if (scrollerLoc > 1920 + 124 + termsScrollBG.height - termsScroller.height) {
					scrollerLoc = 1920 + 124 + termsScrollBG.height - termsScroller.height;
				}
				if (scrollerLoc < 1920 + 124) {
					scrollerLoc = 1920 + 124;
				}
				// println("scrollerMoving = true");
				// println("scrollerOffset = " + scrollerOffset);
				// println("mouseY = " + mouseY);
				// image(termsScroller, 880, scrollerLoc);
				float rat = (float) ((float) (scrollerLoc) - (float) (1920 + 124))
						/ (float) (termsScrollBG.height - termsScroller.height);
				int topNum = scrollerLoc - (1920 + 124);
				// println("topNum = " + topNum);
				// println("termsScroller.height = " + termsScroller.height);
				// println("rat = " + rat);
				textLoc = (int) (1920 - ((termsText.height - 768) * rat));
				// println("textLoc = " + textLoc);
				// println("termsText.height = " + termsText.height);
			}
			// else
			// {

			image(termsText, 0, textLoc);
			image(termsTop, 0, 1920);
			image(termsScrollBG, 880, 124 + 1920);
			image(termsScroller, 886, scrollerLoc);
			// }
		}
	}

	// *********************************************Terms*******************************************//
	void drawTerms() {
		// scale(1, -1);
		if (setBG == true) {
			// setBG = false;
			background(35, 31, 32);
		}

		// rotate(radians(-90));
		if (img != null) {
			image(img, 0, 0);
		}
		if (whichLang == 1) {
			image(thisYour, 0, 0);
			// rotate(radians(0));
			/*
			 * switch (termsPage) { case 1: image(terms1, 0, 1920); break; case
			 * 2: image(terms2, 0, 1920); break; case 3: image(terms3, 0, 1920);
			 * break; case 4: image(terms4, 0, 1920); break; case 5:
			 * image(terms5, 0, 1920); break; case 6: image(terms6, 0, 1920);
			 * break; case 7: image(terms7, 0, 1920); break; case 8:
			 * image(terms8, 0, 1920); break; case 9: image(terms9, 0, 1920);
			 * break; case 10: image(terms10, 0, 1920); break; case 11:
			 * image(terms11, 0, 1920); break; case 12: image(terms12, 0, 1920);
			 * break; case 13: image(terms13, 0, 1920); break; }
			 */
			// image(terms, 0, 1920);
			if (box1Checked == true && box2Checked == true) {
				image(ok, 793, 1920 + 636);
			}
			if (box1Checked == false) {
				image(box1, 271, 1920 + 670);
			} else {
				image(box1che, 271, 1920 + 670);
			}
			if (box2Checked == false) {
				image(box2, 271, 1920 + 711);
			} else {
				image(box2che, 271, 1920 + 711);
			}
			image(exit, 37, 1920 + 636);
		} else {
			image(thisYourS, 0, 0);
			// rotate(radians(0));
			// image(termsS, 0, 1920);
			/*
			 * switch (termsPage) { case 1: image(terms1S, 0, 1920); break; case
			 * 2: image(terms2S, 0, 1920); break; case 3: image(terms3S, 0,
			 * 1920); break; case 4: image(terms4S, 0, 1920); break; case 5:
			 * image(terms5S, 0, 1920); break; case 6: image(terms6S, 0, 1920);
			 * break; case 7: image(terms7S, 0, 1920); break; case 8:
			 * image(terms8S, 0, 1920); break; case 9: image(terms9S, 0, 1920);
			 * break; case 10: image(terms10S, 0, 1920); break; case 11:
			 * image(terms11S, 0, 1920); break; case 12: image(terms12S, 0,
			 * 1920); break; case 13: image(terms13S, 0, 1920); break; }
			 */
			if (box1Checked == true && box2Checked == true) {
				image(okS, 793, 1920 + 636);
			}
			if (box1Checked == false) {
				image(box1, 271, 1920 + 670);
			} else {
				image(box1che, 271, 1920 + 670);
			}
			if (box2Checked == false) {
				image(box2, 271, 1920 + 711);
			} else {
				image(box2che, 271, 1920 + 711);
			}
			image(exitS, 37, 1920 + 636);
		}
	}

	// *********************************************Email*******************************************//
	void drawEmail() {
		// scale(1, -1);
		if (setBG == true) {
			// setBG = false;
			background(35, 31, 32);
		}
		// rotate(radians(-90));
		if (img != null) {
			image(img, 0, 0);
		}

		if (whichLang == 1) {
			image(didYouKnow, 0, 0);
			image(keyboard, 0, 2203);

			image(progress4, 0, 1920);
			image(exit, 60, 2054);
			image(allReq, 0, 1920);
		} else {
			image(didYouKnowS, 0, 0);
			image(keyboard, 0, 2203);

			image(progress4S, 0, 1920);
			image(exitS, 60, 2054);
			image(allReqS, 0, 1920);
		}
		// image(emailBox, 74, 2103);
		// image(fNameBox, 74, 2042);
		// image(lNameBox, 395, 2042);
		// image(emailBoxActive, 271, 2109 - 27);
		switch (whichField) {
		case 1:
			image(emailBoxActive, 271, 2109);
			image(fNameBox, 271, 2054);
			image(lNameBox, 529, 2054);
			break;
		case 2:
			image(emailBox, 271, 2109);
			image(fNameBoxActive, 271, 2054);
			image(lNameBox, 529, 2054);
			break;
		case 3:
			image(emailBox, 271, 2109);
			image(fNameBox, 271, 2054);
			image(lNameBoxActive, 529, 2054);
			break;
		}

		textAlign(LEFT);
		fill(0);
		textFont(f, textSizeE);
		// 630 for email
		// 309 for names
		float ew = textWidth(emailField);
		textFont(f, textSizeF);
		float fw = textWidth(fName);
		textFont(f, textSizeL);
		float lw = textWidth(lName);
		while (ew > 485) {
			textSizeE--;
			textFont(f, textSizeE);
			ew = textWidth(emailField);
		}
		while (fw > 228) {
			textSizeF--;
			textFont(f, textSizeF);
			fw = textWidth(fName);
		}
		while (lw > 228) {
			textSizeL--;
			textFont(f, textSizeL);
			lw = textWidth(lName);
		}
		textFont(f, textSizeE);
		text(emailField, 281, 2145); // -27
		textFont(f, textSizeF);
		text(fName, 281, 2090);
		textFont(f, textSizeL);
		text(lName, 539, 2090);

		// image(emailBox, 271, 2109);
		// image(fNameBox, 271, 2054);
		// image(lNameBoxActive, 529, 2054);

		if (emailField.equals("")) {
			textSizeE = 36;
			textFont(f, textSizeE);
			fill(200);
			if (whichLang == 1) {
				text("EMAIL ADDRESS", 281, 2145);
			} else {
				text("CORREO ELECTRÓNICO ", 281, 2145);
			}
		} else {
			if (whichLang == 1) {
				image(send, 785, 2054);
			} else {
				image(sendS, 785, 2054);
			}
		}

		if (fName.equals("")) {
			textSizeF = 36;
			textFont(f, textSizeF);
			fill(200);
			if (whichLang == 1) {
				text("FIRST NAME", 281, 2090);
			} else {
				text("NOMBRE", 281, 2090);
			}
		}

		if (lName.equals("")) {
			textSizeL = 36;
			textFont(f, textSizeL);
			fill(200);
			if (whichLang == 1) {
				text("LAST NAME", 539, 2090);
			} else {
				text("APELLIDO", 539, 2090);
			}
		}
	}

	// *********************************************Confirm*******************************************//
	void drawConfirm() {
		// scale(1, -1);
		if (setBG == true) {
			// setBG = false;
			background(35, 31, 32);
		}
		// rotate(radians(-90));
		if (img != null) {
			image(img, 0, 0);
		}
		if (whichLang == 1) {
			image(didYouKnow, 0, 0);

			image(confirm, 0, 1920);
			image(progress4, 0, 1920);
			if (hasSent == false) {
				image(yesSend, 611, 2344);
				image(noRetype, 213, 2344);
				image(exit, 60, 2540);
			}
		} else {
			image(didYouKnowS, 0, 0);

			image(confirmS, 0, 1920);
			image(progress4S, 0, 1920);
			if (hasSent == false) {
				image(yesSendS, 611, 2344);
				image(noRetypeS, 213, 2344);
				image(exitS, 60, 2540);
			}
		}
		textFont(f, 36);
		fill(34, 144, 207);
		textAlign(CENTER);
		text(emailField, (1024 / 2), 2200);
		fill(0);
	}

	boolean hasSent = false;

	// *********************************************Congrats*******************************************//
	void drawCongrats() {
		// scale(1, -1);
		if (setBG == true) {
			// setBG = false;
			background(35, 31, 32);
		}
		// rotate(radians(-90));
		if (img != null) {
			image(img, 0, 0);
		}
		// image(thisYour, 0, 0);
		if (whichLang == 1) {
			image(congratsTouch, 0, 1920);
			image(congrats, 0, 610);
			image(exit, 60, 2540);
			image(didYouKnow, 0, 0);
		} else {
			image(congratsTouchS, 0, 1920);
			image(congratsS, 0, 610);
			image(exitS, 60, 2540);
			image(didYouKnowS, 0, 0);
		}
	}

	int pickClosest(PImage image) {

		switch (whichGroup) {
		case 1:
			// distArray = new int[photos_ST.length];

			distArray1 = new int[photos_ST.length];
			distArray2 = new int[photos_ST.length];
			distArray3 = new int[photos_ST.length];
			distArray4 = new int[photos_ST.length];
			distArrayAll = new int[photos_ST.length];
			PImage image1_1 = image.get(0, 0, smallTileWidth / 2, smallTileHeight / 2);
			PImage image2_1 = image.get(smallTileWidth / 2, 0, smallTileWidth / 2, smallTileHeight / 2);
			PImage image3_1 = image.get(0, smallTileHeight / 2, smallTileWidth / 2, smallTileHeight / 2);
			PImage image4_1 = image.get(smallTileWidth / 2, smallTileHeight / 2, smallTileWidth / 2,
					smallTileHeight / 2);

			for (int i = 0; i < photos_ST.length; i++) {
				// int distSum = distanceBetween(image, photos_ST[i], 0);
				// distArray[i] = distSum;

				int distSum1 = distanceBetween(image1_1, photos_ST[i], 1);
				distArray1[i] = distSum1;

				int distSum2 = distanceBetween(image2_1, photos_ST[i], 2);
				distArray2[i] = distSum2;

				int distSum3 = distanceBetween(image3_1, photos_ST[i], 3);
				distArray3[i] = distSum3;

				int distSum4 = distanceBetween(image4_1, photos_ST[i], 4);
				distArray4[i] = distSum4;

				distArrayAll[i] = (distSum1 + distSum2 + distSum3 + distSum4) / 4;
			}
			break;
		case 2:
			// distArray = new int[photos_HC.length];

			distArray1 = new int[photos_HC.length];
			distArray2 = new int[photos_HC.length];
			distArray3 = new int[photos_HC.length];
			distArray4 = new int[photos_HC.length];
			distArrayAll = new int[photos_HC.length];
			PImage image1_2 = image.get(0, 0, smallTileWidth / 2, smallTileHeight / 2);
			PImage image2_2 = image.get(smallTileWidth / 2, 0, smallTileWidth / 2, smallTileHeight / 2);
			PImage image3_2 = image.get(0, smallTileHeight / 2, smallTileWidth / 2, smallTileHeight / 2);
			PImage image4_2 = image.get(smallTileWidth / 2, smallTileHeight / 2, smallTileWidth / 2,
					smallTileHeight / 2);

			for (int i = 0; i < photos_HC.length; i++) {
				// int distSum = distanceBetween(image, photos_HC[i], 0);
				// distArray[i] = distSum;

				int distSum1 = distanceBetween(image1_2, photos_HC[i], 1);
				distArray1[i] = distSum1;

				int distSum2 = distanceBetween(image2_2, photos_HC[i], 2);
				distArray2[i] = distSum2;

				int distSum3 = distanceBetween(image3_2, photos_HC[i], 3);
				distArray3[i] = distSum3;

				int distSum4 = distanceBetween(image4_2, photos_HC[i], 4);
				distArray4[i] = distSum4;

				distArrayAll[i] = (distSum1 + distSum2 + distSum3 + distSum4) / 4;
			}
			break;
		case 3:
			// distArray = new int[photos_AD.length];

			distArray1 = new int[photos_AD.length];
			distArray2 = new int[photos_AD.length];
			distArray3 = new int[photos_AD.length];
			distArray4 = new int[photos_AD.length];
			distArrayAll = new int[photos_AD.length];
			PImage image1_3 = image.get(0, 0, smallTileWidth / 2, smallTileHeight / 2);
			PImage image2_3 = image.get(smallTileWidth / 2, 0, smallTileWidth / 2, smallTileHeight / 2);
			PImage image3_3 = image.get(0, smallTileHeight / 2, smallTileWidth / 2, smallTileHeight / 2);
			PImage image4_3 = image.get(smallTileWidth / 2, smallTileHeight / 2, smallTileWidth / 2,
					smallTileHeight / 2);

			for (int i = 0; i < photos_AD.length; i++) {
				// int distSum = distanceBetween(image, photos_AD[i], 0);
				// distArray[i] = distSum;

				int distSum1 = distanceBetween(image1_3, photos_AD[i], 1);
				distArray1[i] = distSum1;

				int distSum2 = distanceBetween(image2_3, photos_AD[i], 2);
				distArray2[i] = distSum2;

				int distSum3 = distanceBetween(image3_3, photos_AD[i], 3);
				distArray3[i] = distSum3;

				int distSum4 = distanceBetween(image4_3, photos_AD[i], 4);
				distArray4[i] = distSum4;

				distArrayAll[i] = (distSum1 + distSum2 + distSum3 + distSum4) / 4;
			}
			break;
		case 4:
			// distArray = new int[photos_All.length];

			distArray1 = new int[photos_All.length];
			distArray2 = new int[photos_All.length];
			distArray3 = new int[photos_All.length];
			distArray4 = new int[photos_All.length];
			distArrayAll = new int[photos_All.length];
			PImage image1 = image.get(0, 0, smallTileWidth / 2, smallTileHeight / 2);
			PImage image2 = image.get(smallTileWidth / 2, 0, smallTileWidth / 2, smallTileHeight / 2);
			PImage image3 = image.get(0, smallTileHeight / 2, smallTileWidth / 2, smallTileHeight / 2);
			PImage image4 = image.get(smallTileWidth / 2, smallTileHeight / 2, smallTileWidth / 2, smallTileHeight / 2);

			for (int i = 0; i < photos_All.length; i++) {
				// int distSum = distanceBetween(image, photos_All[i], 0);
				// distArray[i] = distSum;

				int distSum1 = distanceBetween(image1, photos_All[i], 1);
				distArray1[i] = distSum1;

				int distSum2 = distanceBetween(image2, photos_All[i], 2);
				distArray2[i] = distSum2;

				int distSum3 = distanceBetween(image3, photos_All[i], 3);
				distArray3[i] = distSum3;

				int distSum4 = distanceBetween(image4, photos_All[i], 4);
				distArray4[i] = distSum4;

				distArrayAll[i] = (distSum1 + distSum2 + distSum3 + distSum4) / 4;
			}
			break;
		}

		int winner = 0;
		boolean doesntMatch = true;
		for (int k = 0; k < distArrayAll.length; k++) {
			for (int v = lastOnes.length - 1; v >= 0; v--) {
				if (lastOnes[v] == k) {
					doesntMatch = false;
				}
			}
			if (distArrayAll[k] < distArrayAll[winner] && doesntMatch == true) {
				winner = k;
			}
			doesntMatch = true;
		}
		// lastOnes[19] = lastOnes[18];

		for (int q = lastOnes.length - 1; q > 0; q--) {
			lastOnes[q] = lastOnes[q - 1];
		}
		lastOnes[0] = winner;

		return winner;
	}

	// loads the main image with the super pixels replaced by the smaller images
	void loadMain() {
		println("loadMain");
		pauseTimer = true;
		notRestarted = true;
		timeoutTimer = 0;
		mosaicing = true;
		for (int x = 0; x < smallHeight; x += smallTileHeight) {
			for (int y = 0; y < smallWidth; y += smallTileWidth) {
				PImage cur = tiny.get(x, y, smallTileHeight, smallTileWidth);
				int tempX = y * tileWidth / 2;
				int tempY = x * tileHeight / 2;
				theBoxes.add(new Box(cur, tempX, tempY));
			}
		}

		// println("done picking");
		animIsStarted = true;

	}
	
	private ArrayList<Box> shuffleArray(ArrayList<Box> arr)
	{
		ArrayList<Box> shuffledArray = new ArrayList<Box>();
		while(arr.size() > 0)
		{
			int rand = Math.round(random(0, arr.size() - 1));
			if (rand < theBoxes.size() && arr.get(rand) != null)
			{
				shuffledArray.add(new Box(arr.get(rand).img, arr.get(rand).theX, arr.get(rand).theY));
				arr.remove(rand);
			}
		}
		
		return shuffledArray;
	}




	void allDone() {
		println("allDone");
		stopDraw = false;
		if (notRestarted == true) {
			fileName = "mosaic_" + year() + month() + day() + hour() + minute() + second() + millis() + ".png";
			System.out.println("Saved file names: " + fileName);
			img = get(0, 0, 1080, 1920);


			img.save("output/" + fileName);
			PImage pg = createGraphics(varsXML.outputSizeWidth, varsXML.outputSizeHeight);
			pg.get();
			loadPixels();
			pg.copy(img, 0, 0, 1080, 1920, 0, 0, varsXML.outputSizeWidth, varsXML.outputSizeHeight);
			pg.updatePixels();

			fileName = "small_" + fileName;
			pg.save("output/" + fileName);

			scrollerLoc = 1920 + 124;
			textLoc = 1920;
			currentScreen++;
			notRestarted = false;
			whichFact = 0;
			
		} 

		
		mosaicing = false;
		pauseTimer = false;
	}

	class ImageURL {
		PImage img;
		boolean loaded = false, error = false;
		String url;

		public ImageURL(String _url) {
			url = _url;
		}

		void load() {
			println("class ImageURL, load");
			// println("\nLoading image: "+url);
			img = loadImage(url);
			if (img == null)
				error = true;
			loaded = true;
			if (error) {
				println("Error occurred when loading image.");
			} else {
				// println("Load successful. "+ "Image size is
				// "+img.width+"x"+img.height+".");
			}
		}
	}

	// calculates the distance between the colors of two images
	int distanceBetween(PImage i1, Photo i2, int which) {
		float accumSum = 0;

		float sumRed1 = 0;
		float sumGreen1 = 0;
		float sumBlue1 = 0;

		for (int i8 = 0; i8 < i1.pixels.length; i8++) {
			sumRed1 += red(i1.pixels[i8]);
		}
		sumRed1 = sumRed1 / i1.pixels.length;

		for (int i4 = 0; i4 < i1.pixels.length; i4++) {
			sumGreen1 += green(i1.pixels[i4]);
		}
		sumGreen1 = sumGreen1 / i1.pixels.length;

		for (int i6 = 0; i6 < i1.pixels.length; i6++) {
			sumBlue1 += blue(i1.pixels[i6]);
		}
		sumBlue1 = sumBlue1 / i1.pixels.length;

		switch (which) {
		case 0:
			accumSum = dist(sumRed1, sumGreen1, sumBlue1, i2.r, i2.g, i2.b);
			break;
		case 1:
			accumSum = dist(sumRed1, sumGreen1, sumBlue1, i2.r1, i2.g1, i2.b1);
			break;
		case 2:
			accumSum = dist(sumRed1, sumGreen1, sumBlue1, i2.r2, i2.g2, i2.b2);
			break;
		case 3:
			accumSum = dist(sumRed1, sumGreen1, sumBlue1, i2.r3, i2.g3, i2.b3);
			break;
		case 4:
			accumSum = dist(sumRed1, sumGreen1, sumBlue1, i2.r4, i2.g4, i2.b4);
			break;
		}

		return round(accumSum);
	}

	// darkens (or lightens if given a decimal factor) the pixels of the image
	PImage darken(PImage in, float factor) {
		PImage out = createImage(in.width, in.height, RGB);
		for (int i = 0; i < in.width * in.height; i++) {
			out.pixels[i] = color(red(in.pixels[i]) / factor, green(in.pixels[i]) / factor,
					blue(in.pixels[i]) / factor);
		}
		return out;
	}

	void loadData() {
		println("class ImageURL, loadData");
		// Load XML file
		println("loadData");
		//xml_ST = loadXML("data_ScienceTechnology.xml");
		// Get all the child nodes named "bubble"
		XML[] children = xml_ST.getChildren("image");

		// The size of the array of Bubble objects is determined by the total
		// XML elements named "bubble"
		photos_ST = new Photo[children.length];

		for (int i = 0; i < photos_ST.length; i++) {

			// XML positionElement = children[i].getChild("fileName");
			// String fn = positionElement.getContent;
			String fn = "null";
			XML redElement = children[i].getChild("red");
			float r = redElement.getFloatContent();

			XML greenElement = children[i].getChild("green");
			float g = greenElement.getFloatContent();

			XML blueElement = children[i].getChild("blue");
			float b = blueElement.getFloatContent();

			// photos[i] = new Photo(fn, r, g, b);
			////////////////////////////////////////////
			XML redElement1 = children[i].getChild("red1");
			float r1 = redElement1.getFloatContent();

			XML greenElement1 = children[i].getChild("green1");
			float g1 = greenElement1.getFloatContent();

			XML blueElement1 = children[i].getChild("blue1");
			float b1 = blueElement1.getFloatContent();
			////////////////////////////////////////////
			XML redElement2 = children[i].getChild("red2");
			float r2 = redElement2.getFloatContent();

			XML greenElement2 = children[i].getChild("green2");
			float g2 = greenElement2.getFloatContent();

			XML blueElement2 = children[i].getChild("blue2");
			float b2 = blueElement2.getFloatContent();
			////////////////////////////////////////////
			XML redElement3 = children[i].getChild("red3");
			float r3 = redElement3.getFloatContent();

			XML greenElement3 = children[i].getChild("green3");
			float g3 = greenElement3.getFloatContent();

			XML blueElement3 = children[i].getChild("blue3");
			float b3 = blueElement3.getFloatContent();
			////////////////////////////////////////////
			XML redElement4 = children[i].getChild("red4");
			float r4 = redElement4.getFloatContent();

			XML greenElement4 = children[i].getChild("green4");
			float g4 = greenElement4.getFloatContent();

			XML blueElement4 = children[i].getChild("blue4");
			float b4 = blueElement4.getFloatContent();

			photos_ST[i] = new Photo(fn, r, g, b, r1, g1, b1, r2, g2, b2, r3, g3, b3, r4, g4, b4);
		}
	}

}
